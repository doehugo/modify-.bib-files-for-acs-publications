Script that can modify .bib files. This includes options to:
- Make paper titles title case or lower case (latter not yet implemented)
- Remove curly brackets around the title to let the .sty file decide the layout, while preserving some special words.
- Italize latin words and abbreviations
- Convert the journal titles to CASSI-based abbreviations.
- Add a string with the URL to the 'eprint' category for preprints. The .sty file can be modified to print the content of 'eprint'.

This is intended for publication in:
- an ACS journal, which may demand title case titles and CASSI-based journal abbreviations. 
- a Nature journal, which uses lower case titles and wants the preprint URL shown. When using biblatex, this is done automatically with the 'nature' style. But with biblatex (compulsory for RevTex4-1), you need to use this hack. 

The abbreviations are based on a list of journals and their abbreviations that 
are copied from https://woodward.library.ubc.ca/research-help/journal-abbreviations/ 
(last updated: Tuesday May 3, 2022).

Please feel free to add to this database. 

To use, download the python script and database and put in one folder with the 
bib file to change. The testreferences.bib file is for testing on a dummy.