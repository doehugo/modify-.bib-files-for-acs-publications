#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug 25 13:51:32 2020

@author: hugodoeleman

Converting bib files for different formatting.
This script can change the titles to be title case or lower 
case (lower case not yet implemented), or it can remove curly 
brackets around the title to let the .sty file decide the layout.

Optionally also puts latin in italics.
Optionally also abbreviates journal titles.
Optionally also add a string with the URL to the 'eprint' category for preprints.

"""

import regex as re
from copy import deepcopy as dcopy
import pandas as pd
import numpy as np
from itertools import product

### Parameters
filename="testreferences.bib"
#what to do with titles. Only one of these booleans should be true.
make_titlecase=False #make title case (bool)
make_lowercase=False #make lower case (bool) - not yet implemented
remove_extra_brackets=True #remove extra brackets around titles (bool). 
#other options
italize_latin=True #put latin in italics or not.
abbrev= False # replace journal names with abbreviations
make_preprint_str= False #for preprints, add a string with the URL in the 'eprint' category. Also adds empty 'journal' string.
arxiv_to_unpublished= True #convert arxiv preprints to the 'unpublished category' and add an empty note.

### Library of journal abbreviations
### Source: https://woodward.library.ubc.ca/research-help/journal-abbreviations/ (last updated: Tuesday May 3, 2022)
abbrev_db= pd.read_csv('All_abbrev.txt', sep='\t', header=0, dtype='str', 
                        encoding='utf8', engine='python')

### Define some lists of words that need special treatments
# list of articles 
articles = ["a", "an", "the"] 
    
# list of coordinating conjunctins 
conjunctions = ["and", "but", 
                "for", "nor", 
                "or", "so", 
                "yet"] 
    
# list of some short articles 
prepositions = ["in", "to", "for",  
                "with", "on", "at", 
                "from", "by", "about", 
                "as", "into", "like", 
                "through", "after", "over", 
                "between", "out", "against",  
                "during", "without", "before", 
                "under", "around", "among", 
                "of"] 

# latin terms that should be lower case. Make sure to escape regex special characters
latin_single= ["via", "i\.e\.", "e\.g\.", "vs\.","etc\."]

#multi-word latin
latin_multi=[ "in situ", "in-situ", "in vitro", "in-vitro", 
                "a priori", "a-priori", "ab initio", "ab-initio",
                "et al\."] 

#names (need to be capitalized)
names=["Bell", "Fock", "Brillouin"]
names_donottouch=['Schr{\\"{o}}dinger', 'Q'] #in stead of capitalizing these names, keep them as is. Because they give issues when making title case. Also include anything else that should be kept as is.

#units (need to remain as they are)
unit_prefixes=['f','p','n','m','d','k','M','G','T','P'] #femto, pico etc. (micro is omitted because math is escaped anyway)
unit_bases= ["Hz", "g", "m", "J", "W"] #units
units=[combo[0]+combo[1] for combo in product(unit_prefixes,unit_bases)] #all combinations of prefixes and bases

### functions
def generateTitleCase(input_string, italize_latin=True): 
    """ Function to convert into title case.
        Also capitalizes after a hyphen.
        Optionally also italizes latin.
    """
        
    #words that should be italized
    italize_list=latin_single +latin_multi
    
    #exceptions that are treated separately.
    exeptions_tc= latin_multi #should be title cased
    
    # merging the 3 lists that should not be changed
    untouched = articles + conjunctions + prepositions + latin_single + units + names_donottouch
        
    # variable declaration for the output text  
    output_string = "" 
    
    ## Generate new string
    
    # separating each word in the string 
    input_list = input_string.split(" ") 
      
    # checking each word 
    for word in input_list: 
          
        # do not capitalize these words
        if word in untouched: 
            output_string += word + " "
        
        # if the word is all caps (acronym), don't change it.
        elif word.isupper(): 
            output_string += word + " "
        
        # if the word contains multiple caps and no hypen, I assume it's a 
        # chemical formula and keep it.
        elif (len(re.findall('-',word))==0)&(len(re.findall(r'[A-Z]',word))>1):
            output_string += word + " "
            
        #if there is a hyphen, we check the words individually
        elif (len(re.findall('-',word))!=0): 
            for subword in word.split(sep='-'):
                if len(re.findall(r'[A-Z]',subword))<2: #check if not a chemical formula
                     word=word.replace(subword, subword.title(),1)
            
            output_string += word + " " 
        
        #exceptions for strings with 's in it
        elif ((word.find(r"'s") != -1)|(word.find(r"’s") != -1)|(word.find(r"`s") != -1)):
            sep=[ap for ap in "'`’" if ap in word][0]
            subwords=word.split(sep=sep)
            temp = subwords[0].title()+sep+subwords[1] 
            output_string += temp + " "

        
        # if the word does not exists in 
        # the list, then capitalize it 
        else: 
            temp = word.title() 
            output_string += temp + " "
              
    
    #separately look for some exeptions that should be title case
    for exeption in exeptions_tc:
        pattern = re.compile(exeption, re.IGNORECASE) #to make case insensitive
        output_string=pattern.sub(exeption.title(), output_string) #replace all by title case
    
    #Italize latin
    if italize_latin:
        output_string = italize(output_string,italize_list)

    return output_string[:-1] #remove last space


def protect_1word(word,tc_list,untouched_list):
    """ Function to check a words for eligibility for protection, and surround with brackets if eligible.
        can_have_hyphen means that chemical formulas 
    """

    # if the word should be title case, make so and protect
    if word.lower() in [item.lower() for item in tc_list]: #make it case-insensitive
        word_mod= "{"+word.title()+"}" 
    
    # if the word is all caps (acronym), protect it.
    elif (len(word)>1)&(word.isupper()): 
        word_mod= "{"+word+"}" 
    
    # if the word is not to be touched, protect it.        
    elif word in untouched_list:
        word_mod= "{"+word+"}" 

    # if the word contains multiple caps, I assume it's a 
    # chemical formula and protect it.
    elif len(re.findall(r'[A-Z]',word))>1:
        word_mod= "{"+word+"}" 

    else: #leave it unprotected
        word_mod=word
    
    return word_mod

def protect_special_cases(input_string, italize_latin=True): 
    """ Function to surround certain words in the title with brackets to protect them from modification.
        Optionally also italizes latin.
    """
        
    #words that should be italized
    italize_list=latin_single +latin_multi
    
    #words that should be title case
    title_case = names

    #words that should remain untouched
    untouched= units + names_donottouch

    # variable declaration for the output text  
    output_string = "" 
    
    ## Generate new string
    
    # separating each word in the string 
    input_list = input_string.split(" ") 
      
    # checking each word 
    for word in input_list: 
        
        #if there is a hyphen, we check the words individually
        if (len(re.findall('-',word))!=0): 
            word_mod=word
            for subword in word.split(sep='-'): 
                subword_mod= protect_1word(subword,title_case,untouched)
                word_mod=word_mod.replace(subword, subword_mod,1)
        
        #split strings with 's in it
        elif ((word.find(r"'s") != -1)|(word.find(r"’s") != -1)|(word.find(r"`s") != -1)):
            sep=[ap for ap in "'`’" if ap in word][0]
            subwords=word.split(sep=sep)
            word_mod = protect_1word(subwords[0],title_case,untouched)+sep+subwords[1] 

        else: #normal words, not split
            word_mod = protect_1word(word,title_case,untouched)

        output_string += word_mod + " " 
        
    #Italize latin and protect
    if italize_latin:
        output_string = italize(output_string,italize_list)
    
    return output_string[:-1] #remove last space

def journal_abbrev(input_string):
    "finds journal abbreviation in database"
    if any(abbrev_db['Journal'] == input_string):
        abbrev=abbrev_db['Abbreviation'][abbrev_db['Journal']==input_string].iloc[0]
    else:
        abbrev=np.nan
    return abbrev
        
#Italize latin and protect with brackets
def italize(input_string,italize_list):
    output_string=dcopy(input_string)
    for it_str in italize_list:
        pattern = re.compile(it_str, re.IGNORECASE) #to make case insensitive
        for match in pattern.finditer(output_string):
            word=match.group() 
            output_string=output_string.replace(word, r"{\textit{"+word+"}}")  #note that this fails if word occurs more than once.
    return output_string














### import file
with open(filename,'r',encoding='utf8') as f_open:
    filestr = f_open.read() #this becomes one long string


### find titles and convert, either to title or lower case, or remove the curly brackets to let the sty file decide.
converted_titles=[]
for match in set(re.finditer("\\n\s*title\s*=\s*\{(.+)\},\\n", filestr)):
    
    title=match.group(1)

    #remove extra curly brackets around title, if present
    if (title[0]=='{')&(title[-1]=='}'):
        title_mod=title[1:-1] 
    else:
        title_mod=title

    if title not in converted_titles: #in case there are duplicate titles, I don't want to convert twice.    

        if make_titlecase: #convert to title case

            title_mod=generateTitleCase(title_mod, italize_latin=italize_latin) #convert

        elif make_lowercase: #convert to lower case (except first word)
            
            title_mod=title_mod #convert - to be written later

        if make_titlecase|make_lowercase:
            #make sure that there are double curly brackets around the title. Necessary to preserve the modifications.
            title_mod="{"+title_mod+"}" 

        if remove_extra_brackets: #we keep the extra brackets away, so that the .sty file will decide on the layout
            
            #Protect the special cases who's layout should be preserved
            title_mod=protect_special_cases(title_mod, italize_latin=italize_latin)
        elif not make_titlecase|make_lowercase: #return the extra brackets, if they were there
            if (title[0]=='{')&(title[-1]=='}'):
                title_mod='{'+title_mod+'}'

        # print(title)
        # print(title_mod)
        filestr=filestr.replace(match.group(0),match.group(0).replace(title,title_mod)) #replace title with title_mod. This avoids replacing the title in other places (like the booktitle).

        #append to converted titles
        converted_titles.append(title)
    
### find journal titles and convert to abbreviations
if abbrev:
    journals_done=[]
    for match in re.finditer("\s+journal\s+= \{(.+)\},\\n", filestr):
        
        journal=match.group(1)
#        print(journal)
        if journal not in journals_done: #avoid double work
            abbrev=journal_abbrev(journal) #convert
            if (isinstance(abbrev, str))&(abbrev!=""):
                filestr=filestr.replace(journal, abbrev) #replace all instances
            
            journals_done.append(journal)    


### For preprints, place a string with the URL in the 'eprint' catagory
### The style file can be modified to print that string.
if make_preprint_str:
    for entry_match in re.finditer("@article\{(.+?)\\n\}", filestr,flags=re.DOTALL): #finds library entries
        
        entry=entry_match.group(1)

        url_match=re.search("\s+url\s+= \{(.+)\},\\n", entry)
        if (re.search("\s+journal\s+= \{(.+)\},\\n", entry)==None) & (url_match!=None) : #if no journal is specified and there is a URL, I presume it's a preprint.
            url=url_match.group(1)
            eprint_content="Preprint at "+url 

            eprint_match=re.search("\s+eprint\s+= \{(.+)\},\\n", entry)
            if eprint_match: #if an eprint entry existed, we replace it.
                eprint_content_orig=eprint_match.group(1)
                entry_mod=entry.replace("eprint = {"+eprint_content_orig+"}", "eprint = {"+eprint_content+"}")

            else: #no eprint entry existed, so we append it
                eprint_fullstr=",\neprint = {"+eprint_content+"}"
                entry_mod=entry+eprint_fullstr

            #now also append an empty 'journal'
            journal_str=",\njournal = {}"
            entry_mod=entry_mod+journal_str

            filestr=filestr.replace(entry, entry_mod) #replace entry with updated version. Entries must be unique because labels are unique

### For preprints, just convert to 'unpublished'. Also add an empty note. This works with RevTex4.2.
if arxiv_to_unpublished:
    for entry_match in re.finditer("(@article|@unpublished)\{(.+?)\\n\}", filestr,flags=re.DOTALL): #finds library entries
        
        entry=entry_match.group(2)
        fullentry=entry_match.group(0)

        url_match=re.search("\s+url\s+= \{(.+)\},\\n", entry)
        if ("@unpublished" in fullentry) | ((re.search("\s+journal\s+= \{(.+)\},\\n", entry)==None) & (url_match!=None)) : #if no journal is specified and there is a URL, I presume it's a preprint.
            fullentry_mod=fullentry

            if "@article" in fullentry:
                fullentry_mod=fullentry_mod.replace("@article", "@unpublished") #convert from article to 'unpublished'

            if (re.search("\s+note\s+= \{(.+)\},\\n", entry)==None): #if no note exists, append a note
                note_str=",\nnote = { }"
                entry_mod=entry+note_str 
                fullentry_mod=fullentry_mod.replace(entry,entry_mod)

            filestr=filestr.replace(fullentry, fullentry_mod) #replace fullentry with updated version. Entries must be unique because labels are unique

### Save
[fn, ext] = filename.split(sep='.')
newfile= open(fn+'_mod.'+ext,"w+", encoding='utf8')
n=newfile.write(filestr)
newfile.close()

